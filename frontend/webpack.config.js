const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const sourcePath = path.resolve(__dirname, 'src');
const staticsPath = path.resolve(__dirname, 'public');


module.exports = {
    context: sourcePath,
    entry: {
        main: './index',
        vendor: ['react', 'react-dom', 'react-router-dom']
    },
    output: {
        path: staticsPath,
        // filename: '[name].[chunkhash].js'
        filename: '[name].js',
        publicPath: '/cabinet/static/'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            loader: 'babel-loader',
            include: sourcePath,
            exclude: /(node_modules|bower_components)/,
            options: {
                presets: ['env', 'react'],
                cacheDirectory: true
            }
        }, {
            test: /\.(scss|css)$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader", // creates style nodes from JS strings
                use: [{
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "sass-loader" // compiles Sass to CSS
                }]
            })
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor', 'manifest'] // Specify the common bundle's name.
        }),
        new ExtractTextPlugin("styles.css"),

        new HtmlWebpackPlugin({
            template: 'index.ejs'
        })

        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'vendor',
        //     // names: ['vendor', 'manifest'],
        //     // chunks: [],
        //     minChunks: function (module) {
        //         // this assumes your vendor imports exist in the node_modules directory
        //         return module.context && module.context.indexOf('node_modules') !== -1;
        //     }
        // }),
        // //CommonChunksPlugin will now extract all the common modules from vendor and main bundles
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'manifest' //But since there are no more common modules between them we end up with just the runtime code included in the manifest file
        // })
    ]
};