import React from 'react'
import {render} from 'react-dom'
import App from './App'
import './reset.css'
import './main.scss'



render(
    <App />,
    document.getElementById('root')
);
