import * as React from "react";
// import React from "react";
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Profile from './modules/profile';
import Titles from './modules/titles';
import Tags from './modules/tags';
import Posts from './modules/posts';
import Dashboard from './modules/dashboard';

class App extends React.Component {
    render() {
        return (
            <Router basename="/cabinet">
                <Route>
                    <div className="page">
                        <div className="header">
                            <Link to="/">Dashboard</Link>
                            <Link to="/posts">Posts</Link>
                            <Link to="/titles">Titles(aka `notes`)</Link>
                            <Link to="/tags">Tags</Link>
                            <Link to="/profile">Profile</Link>
                        </div>
                        <div className="content">
                            <Route exact={true} path="/" component={Dashboard}/>
                            <Route path="/profile" component={Profile}/>
                            <Route path="/titles" component={Titles}/>
                            <Route path="/posts" component={Posts}/>
                            <Route path="/tags" component={Tags}/>
                        </div>
                        <div className="footer">footer</div>
                    </div>
                </Route>
            </Router>
        );
    }
}

export default App;