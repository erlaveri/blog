from flask import Flask
from flask import flash
from flask import redirect
from flask import render_template
from flask import request, jsonify, _request_ctx_stack
from flask import send_from_directory
from flask import session
from flask import url_for
from flask.views import MethodView
from flask_login import LoginManager, login_user
from forms import LoginForm, RegistrationForm
from models import Post, User
from werkzeug.debug import DebuggedApplication
from werkzeug.routing import BaseConverter

app = Flask(__name__)
app.debug = True
# debug - whether to enable debug mode and catch exceptions
# use_debugger - whether to use the internal Flask debugger
# use_reloader - whether to reload and fork the process on exception
# threaded=True
# processes=10
app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
app.secret_key = """\xa5\x1eNmT8\x9b\x92\x88\x12\xf0\xde"3`\x07o{\xd6\xfa\xcbK\xf9\x0c"""


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app.url_map.converters['regex'] = RegexConverter

# app.response_class = MyResponse

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    user_id += 1
    user = User.get_by_id(user_id)
    return user


@app.route('/')
def feed():
    posts = Post.query()

    return render_template('feed.html', posts=posts)


@app.route('/post/<slug>-<post_id>')
def post(slug, post_id):
    post_item = Post.get_by_id(int(post_id))
    return render_template('post.html', post=post_item)


class PostAPI(MethodView):
    def get(self):
        Post.query()
        return ''

    def post(self):
        text = request.form.get('text')
        post_item = Post(text=text)
        key = post_item.put()
        key_id = key.id()

        resp_data = {'id': key_id}
        response = jsonify(resp_data)
        response.status_code = 201

        return response


app.add_url_rule('/api/posts/', view_func=PostAPI.as_view('posts'))


@app.route('/den')
def den():
    return render_template('den.html')


@app.route('/den/posts/edit')
def post_edit():
    return render_template('edit_post.html')


@app.route('/den/posts')
def cabinet_posts():
    posts = Post.query()
    return render_template('cabinet_posts.html', posts=posts)


#
# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == 'POST':
#         session['email'] = request.form['email']
#         return redirect('/')
#     return render_template('login.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(csrf_enabled=False)
    if form.validate_on_submit():
        user = User.query(User.email == form.email.data).get()

        login_user(user)

        flash('Logged in successfully.')

        next = request.args.get('next')
        # is_safe_url should check if the url is safe for redirects.
        # See http://flask.pocoo.org/snippets/62/ for an example.
        # if not is_safe_url(next):
        #     return abort(400)

        return redirect(next or url_for('feed'))
    return render_template('login.html', form=form)


@app.route('/users')
def users_view():
    users = User.query()
    return render_template('users.html', users=users)


# @app.route('/cabinet')
# def cabinet():
#     return send_from_directory('frontend/public', 'index.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(csrf_enabled=False)
    if request.method == 'POST':
        if form.validate_on_submit():
            dd = 5
        email = request.form.get('email')
        password = request.form.get('password')

        if all([email, password]):
            user = User(email=email)
            user.set_password(password)
            user.put()

    return render_template('register.html')
