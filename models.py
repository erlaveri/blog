from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from flask import url_for
from google.appengine.ext import ndb


class Post(ndb.Model):
    timestamp = ndb.DateTimeProperty(auto_now=True, auto_now_add=True)
    name = ndb.StringProperty()
    text = ndb.TextProperty()

    def get_url(self):
        return url_for('post', slug=self.name or 'post', post_id=self.key.id())

    def get_edit_url(self):
        return url_for('post_edit', slug=self.name or 'post', post_id=self.key.id())


class Category(ndb.Model):
    name = ndb.StringProperty()


class User(ndb.Model, UserMixin):
    email = ndb.TextProperty(indexed=True)
    password = ndb.TextProperty()

    def get_id(self):
        return self.key.id()

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)
